## document level event extraction
Document-Level Event Extraction (DEE) 是一种从整个文档层面提取事件的方法。与 Sentence-Level Event Extraction 不同的是，DEE 需要处理论元分散和多事件等问题。

主要贡献：

1. Doc2EDAG: 开创性的提出了篇章级事件抽取的完整解决方案，取得了当时最佳的结果。
2. DEE-PPN: 提出了平行预测网络框架，通过使用多粒度解码器分别对实体和事件角色进行解码，在综合解码器之后，连接到下游预测任务。还引入了匹配损失以优化最终预测m个事件和实际k个事件的任务。
3. DEE-GIT: 引入异质图交互模型和追踪器，利用图谱相关的信息进行节点编码，用于后续的类型检测和记录提取。

不同的方法有各自的创新点和改进之处，其中 Doc2EDAG 是开山之作，DEE-PPN 和 DEE-GIT 在此基础上进行了优化和提高。

